/*
 * mouseHandling.js
 * Copyright (C) 2017 david <david@davidsArch>
 *
 * Distributed under terms of the MIT license.
 */
function allowDrop(ev) {
	ev.preventDefault();
}

function drag(ev) {
	var selectedTarget = $(ev.target).closest("li");
	ev.originalEvent.dataTransfer.setData("text", selectedTarget[0].id);
}

function drop(ev) {
	ev.preventDefault();
	var data = ev.dataTransfer.getData("text");
	var selectedTarget = $(ev.target).closest("li");

	var itemCenterY = $(selectedTarget).height();
	var targetY = (ev.pageY - $(selectedTarget).offset().top);

	if (selectedTarget[0].tagName === "LI") {
		if (targetY > itemCenterY) {
			$("#" + data).insertAfter($(selectedTarget));
		} else {
			$("#" + data).insertBefore($(selectedTarget));
		}
	}
}
