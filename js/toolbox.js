/*
 * toolbox.js
 * Copyright (C) 2017 david <david@davidsArch>
 *
 * Distributed under terms of the MIT license.
 */
function saveListItem() {
	var listItemContainer = $("<div>", { "class": "row" });
	var listItemCount = $("#list1 li").length;
	var itemDescription = $("#contextField");
	var listItemContext = "";

	if ($("#dateCheckbox").is(":checked")) {
		listItemContext = $("#dateInputField").val();
	}

	var newItem = $("<li>", { id: "item" + listItemCount++, "class": "list-group-item list-group-item-action container" });

	var descriptionColumn = $("<div>", { "class":"col", draggable:"true" });
	$(descriptionColumn).on("dragstart", function(event) { drag(event); });

	if (itemDescription.val() === "") {
		descriptionColumn.text(itemDescription[0].placeholder);
	} else {
		descriptionColumn.text(itemDescription.val());
	}

	if ($("#counterCheckbox").is(":checked")) {
		var counterName = $("#counterHeader");

		if (counterName.val() === "") {
			listItemContext += generateCounter(counterName[0].placeholder, listItemCount);
		} else {
			listItemContext += generateCounter(counterName.val(), listItemCount);
		}
	}

	var buttonContainer = $("<div>", { "class": "col doneBtnCol" });
	var doneButton = $("<button>", { "class": "btn btn-success btn-lg" });
	var doneButtonGlyph = $("<span>", { "class": "glyphicon glyphicon-ok-sign" });

	$(doneButton).append(doneButtonGlyph[0].outerHTML + " Done");

	$(buttonContainer).append(doneButton);

	listItemContainer.append(descriptionColumn);

	listItemContainer.append(listItemContext);

	listItemContainer.append(buttonContainer);

	newItem.append(listItemContainer)

	$("#list1").append(newItem);
}

function toggleShowDate(currentElement) {
	$("#toolDateSelect").toggle();
}

function toggleEditMode() {
	$(".tabName").prop('disabled', function(i,v) { return !v; });
	$(".listTab span").toggle();
}

function generateCounter(counterName, id) {
	var counterContainer = $("<div>", { "class": "col counterContainer", "white-space":"nowrap" });
	var counterLabel = $("<label>", { for: "amountInput", "class": "col col-form-label" });
	var counter = $("<input>", { id: "counter" + id++, "class": "form-control col-6", type: "number" });

	counter.val(0);
	counterLabel.text(counterName + ":");

	counterContainer.append(counterLabel);
	counterContainer.append(counter);

	return counterContainer[0].outerHTML;
}

