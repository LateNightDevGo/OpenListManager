/*
 * tabList.js
 * Copyright (C) 2017 david <david@davidsArch>
 *
 * Distributed under terms of the MIT license.
 */
var hidWidth;
var scrollBarWidths = 40;

function AddNewList(ev) {
	var numberOfTabs = $("#tabList li").length -1;
	var newTab = $("<li>", { "class": "nav-item listTab" });
	var newTabLink = $("<a>", { "class":"nav-link", "data-toggle":"tab", "href":"#tab" + numberOfTabs, role:"tab" });

	var tabName = $('<input>', { "class": "tabName", type:"input", placeholder: "Tab " + numberOfTabs });
	var editSpan = $('<span>', { "class": "fa fa-pencil", style:"display:none;" });

	newTabLink.append(tabName);
	$(tabName).prop('disabled', true);
	newTabLink.append(editSpan);
	newTab.append(newTabLink);

	var selectedTarget = $(ev).closest("li");
	$(newTab).insertBefore(selectedTarget);

	reAdjust();
}

var widthOfList = function() {
	var itemsWidth = 0;
	$("#tabList li").each(function() {
		var itemWidth = $(this).outerWidth();
		itemsWidth += itemWidth;
	});

	return itemsWidth;
}

var widthOfHidden = function() {
	return (($(".tabListWrapper").outerWidth())-widthOfList()-getLeftPosition())-scrollBarWidths;
}

function getLeftPosition() {
	return $("#tabList").offset().left;
};

var reAdjust = function() {
	var wrapperSize = $(".tabListWrapper").outerWidth();
	var currentListWidth = widthOfList();
	if (($(".tabListWrapper").outerWidth()) < widthOfList()) {
		$(".scroller-right").show();
	} else {
		$(".scroller-right").hide();
	}

	if (getLeftPosition() < 0) {
		$(".scroller-left").show();
	} else {
		$(".item").animate({left:"-=" + getLeftPosition() + "px"},"slow");
		$(".scroller-left").hide();
	}
}

reAdjust();

$(window).on("resize", function(e) {
	reAdjust();
});

function scrollRight() {
	$(".scroller-left").fadeIn("slow");
	$(".scroller-right").fadeOut("slow");

	$("#tabList").animate({left:"-=" + getLeftPosition() + "px"}, "slow", function() {});
}

function scrollLeft() {
	$(".scroller-right").fadeIn("slow");
	$(".scroller-left").fadeOut("slow");

	$("#tabList").animate({left:"-=" + widthOfHidden() + "px"}, "slow", function() {});
};
